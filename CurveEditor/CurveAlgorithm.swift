//
//  CurveAlgorithm.swift
//  CurveEditor
//
//  Created by Dishan Fernando on 1/9/18.
//  Copyright © 2018 IronOne Technologies. All rights reserved.
//

import UIKit
import Surge


/// Given a set of data points, this class calculate and provides set of control points
/// that can be used to draw a smooth closed curve that goes through all of those
/// points.
/// The logic is based on the nice article provided at below link
/// https://medium.com/@ramshandilya/draw-smooth-curves-through-a-set-of-points-in-ios-34f6d73c8f9
class CurveAlgorithm: NSObject {
    
    
    /// Calculate the control points needed to draw a smooth curve that goes accross the given points.
    /// Given the points [P0, P1, P2, ..., Pi,... Pn]. This method returns a set of control points as below.
    /// [(C0, D0), (C1, D1), ..., (Ci, Di), ... (Cn, Dn)].
    /// here Ci -> First control point for the curve between points Pi and P(i+1).
    ///      Di -> Second control point for the curve between points Pi and P(i+1)
    ///
    /// - Parameter dataPoints: data points of the curve
    /// - Returns: control points for the curve.
    func calculateControlPoints(dataPoints: [CGPoint]) -> [(CGPoint, CGPoint)] {
        let count = dataPoints.count
        precondition(count >= 3, "At leaset 3 data points should exists.")
        
        /************************************* LOGIC **********************************
         A bezier curve between points Pi and Pi+2 is given by following formula
         
         B(t) = (1-t)^3*Pi + 3t(1-t)^2*Ci + 3(1-t)t^2*Di + t^3*P(i+1)
         
         Here Ci and Di are control points. 't' varies from 0 to 1 drawing the curve from
         Pi to P(i+1)
         
         Control points should satisfy following formulas for the curve to go smoothly accross every point
         P1, P2, ... Pi, ... Pn
         
         C(i-1) + 4*Ci + C(i+1) = 4*Pi + 2*P(i+1) ----- (1)
         Di = 2*P(i+1) - Ci --------------------------- (2)
         
         We are using formula (1) to calculate all Ci values and use formula (2) calculate Di values next.
         
         We find Ci values as below.
         By applying values of each data point to formula (1) we obtain following set of equations
         
         4*C0 + 1*C1 + 0*C2 + 0*C3 + ... + 1*Cn = 4*P0 + 2*P1
         1*C0 + 4*C1 + 1*C2 + 0*C3 + ... + 0*Cn = 4*P1 + 2*P2
         0*C0 + 1*C1 + 4*C2 + 1*C3 + ... + 0*Cn = 4*P2 + 2*P3
          .      .      .      .            .      .      .
          .      .      .      .            .      .      .
          .      .      .      .            .      .      .
         1*C0 + 0*C1 + 0*C2 + 1*C3 + ... + 4*Cn = 4*Pn + 2*P0
         
         We can use matrix multiplication to solve the Ci values from here on
         The matrix involved will be
         
         | 4 1 0 0 0 ... 0 1 |   | C0     |   | 4*P0 + 2*P1     |
         | 1 4 1 0 0 ... 0 0 |   | C1     |   | 4*P1 + 2*P2     |
         | 0 1 4 1 0 ... 0 0 |   | C2     |   | 4*P2 + 2*P3     |
         | 0 0 1 4 1 ... 0 0 |   | C3     |   | 4*P3 + 2*P4     |
         |     .          .  | * | .      | = |      .          |
         |     .          .  |   | .      |   |      .          |
         | 0 0 0 0 0 ... 4 1 |   | C(n-1) |   | 4*P(n-1) + 2*Pn |
         | 1 0 0 0 0 ... 1 4 |   | Cn     |   | 4*Pn + 2*P0     |
         
                   A           *     C      =          B
         
         Then
         
         C = Inv(A) * B
         
         After solving C, we use equation (2) to find the D values.
         *********************************************************************************/
        
        let c1Points = calculateC1(dataPoints: dataPoints)
        let c2Points = calculateC2(dataPoints: dataPoints, c1Points: c1Points)
        
        var segments = [(CGPoint, CGPoint)]()
        for i in 0..<count {
            segments.append((c1Points[i], c2Points[i]))
        }

        return segments
    }
    
    
    /// Calculate the coefficient matrix for the Ci values
    /// NOTE: This matrix only depend on the point count. The individual point values doesn't
    ///       matter. Hence we may pre calculate this matrix and its inverse for several values
    ///       and cache for improved peformance
    ///
    /// - Parameter pointCount: number of the points
    /// - Returns: Coefficient matrix for solving Ci values
    private func getCoefficientMatrix(pointCount: Int) -> [[Float]] {
        var data = [[Float]]()
        for i in 0..<pointCount {
            var row = [Float]()
            for j in 0..<pointCount {
                var val: Float = 0
                
                if (i == j) {
                    val = 4
                }
                
                else if (abs(i - j) == 1 || abs(i - j) == pointCount-1) {
                    val = 1
                }
                row.append(val)
            }
            data.append(row)
        }
        
        return data
    }
    
    /// Calculate the result matrix (B) for the Ci values
    ///
    /// - Parameter points: data points
    /// - Returns: Result matrix
    private func getResultMatrixForC1(points: [CGPoint]) -> [[Float]] {
        let count = points.count
        
        var data = [[Float]]()
        for i in 0..<count {
            let j = (i + 1) % count
            
            let P1 = points[i]
            let P2 = points[j]
            
            let P = P1*4.0 + P2*2.0
            data.append(P.toArray())
        }
        
        return data
    }
    
    /// Given a (nx2) matrix, convert it to a list of CGPoint values
    ///
    /// - Parameter matrix: matrix to convert
    /// - Returns: Converted point values
    private func convertToPoints(matrix: Matrix<Float>) -> [CGPoint] {
        precondition(matrix.columns == 2, "Matrix should have 2 cloumns.")
        var points = [CGPoint]()
        
        for i in 0..<matrix.rows {
            let x = CGFloat(matrix[i, 0])
            let y = CGFloat(matrix[i, 1])
            points.append(CGPoint(x:x, y:y))
        }
        
        return points
    }
    
    private func calculateC1(dataPoints: [CGPoint]) -> [CGPoint] {
        let count = dataPoints.count
        precondition(count >= 3, "At leaset 3 data points should exists.")
        let coefficients = getCoefficientMatrix(pointCount: count)
        let resultC1 = getResultMatrixForC1(points: dataPoints)
        
        let A = Matrix(coefficients)
        let B1 = Matrix(resultC1)
        let C1 = inv(A) * B1 //Control point 1
        
        return convertToPoints(matrix: C1)
    }
    
    private func calculateC2(dataPoints: [CGPoint], c1Points: [CGPoint]) -> [CGPoint] {
        precondition(dataPoints.count == c1Points.count, "Size of data points and control points must be same.")
        var c2Points = [CGPoint]()
        
        let count = dataPoints.count
        for i in 0..<count {
            let j = (i+1) % count
            
            let P1 = dataPoints[j]
            let C1 = c1Points[j]
            
            let C2 = P1*2.0 - C1
            c2Points.append(C2)
        }
        
        return c2Points
    }
    
}

extension CGPoint {
    static func + (left: CGPoint, right: CGPoint) -> CGPoint {
        return CGPoint(x: left.x + right.x, y: left.y + right.y)
    }
    
    static func - (left: CGPoint, right: CGPoint) -> CGPoint {
        return CGPoint(x: left.x - right.x, y: left.y - right.y)
    }
    
    static func * (left: CGPoint, right: CGFloat) -> CGPoint {
        return CGPoint(x: left.x * right, y: left.y * right)
    }
    
    static func * (left: CGPoint, right: Double) -> CGPoint {
        return CGPoint(x: left.x.double * right, y: left.y.double * right)
    }

    func toArray() -> [Float] {
        return [self.x.float, self.y.float]
    }
}

extension CGFloat {
    var double: Double {
        return Double(self)
    }
    
    var float: Float {
        return Float(self)
    }
}
