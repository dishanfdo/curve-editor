//
//  CurveEditorView.swift
//  CurveEditor
//
//  Created by Dishan Fernando on 1/9/18.
//  Copyright © 2018 IronOne Technologies. All rights reserved.
//

import UIKit

class CurveEditorView: UIView {
    
    static let pointRadius: CGFloat = 8.0
    static let controlPointRadius: CGFloat = 5.0
    static let curveWidth: CGFloat = 2.0
    static let touchThreshold: CGFloat = 20.0
    
    let curveAlgorithm = CurveAlgorithm()
    
    var controlPoints : [(CGPoint, CGPoint)]? = nil;
    
    var curvePoints: [CGPoint]? = nil {
        didSet {
            guard let points = curvePoints else { return }
            controlPoints = curveAlgorithm.calculateControlPoints(dataPoints: points)
            setNeedsDisplay()
        }
    }
    
    var showControlPoints: Bool = false
    
    var selectedPoint: Int? = nil

    /*
     Only override draw() if you perform custom drawing.
     An empty implementation adversely affects performance during animation.
     */
    override func draw(_ rect: CGRect) {
        
        guard let points = curvePoints else { return }
        guard let controlPoints = controlPoints else { return }
        
        UIColor.red.setFill()
        for point in points {
            let r = CurveEditorView.pointRadius
            let circleRect = CGRect(x: point.x - r/2, y: point.y - r/2, width: r, height: r)
            let circle = UIBezierPath(ovalIn: circleRect)
            circle.fill()
        }
        
        if (showControlPoints) {
            for (cp1, cp2) in controlPoints {
                let r = CurveEditorView.controlPointRadius
                
                UIColor.blue.setFill()
                var circleRect = CGRect(x: cp1.x - r/2, y: cp1.y - r/2, width: r, height: r)
                var circle = UIBezierPath(ovalIn: circleRect)
                circle.fill()
                
                UIColor.purple.setFill()
                circleRect = CGRect(x: cp2.x - r/2, y: cp2.y - r/2, width: r, height: r)
                circle = UIBezierPath(ovalIn: circleRect)
                circle.fill()
            }
        }
        
        let path = UIBezierPath()
        UIColor.red.setStroke()
        
        for i in 0...points.count {
            
            if (i == 0) {
                path.move(to: points[i])
            }
            
            else {
                let a = i % points.count
                let b = (i - 1) % points.count
                
                let point = points[a]
                let (cp1, cp2) = controlPoints[b]
                
                path.addCurve(to: point, controlPoint1: cp1, controlPoint2: cp2)
            }
            
        }
        path.lineWidth = CurveEditorView.curveWidth
        path.stroke()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let points = curvePoints else { return }
        guard let touch = touches.first else { return }
        let index = isTouchOnPoint(touch: touch, points: points)
        if (index != -1) {
            //have a point
            let point = points[index]
            print("Point touched: (\(point.x), \(point.y))")
            self.selectedPoint = index
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let selectedPointIndex = selectedPoint else { return }
        guard let touch = touches.first else { return }
        
        curvePoints?[selectedPointIndex] = touch.location(in: self)
        controlPoints = curveAlgorithm.calculateControlPoints(dataPoints: curvePoints!)
        setNeedsDisplay()
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.selectedPoint = nil
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.selectedPoint = nil
    }
    
    func isTouchOnPoint(touch: UITouch, points: [CGPoint]) -> Int {
        for i in 0..<points.count {
            let point = points[i]
            let touchPoint = touch.location(in: self)
            let distSquared = touchPoint.distanceSquaredTo(point: point)
            if (distSquared < CurveEditorView.touchThreshold) {
                return i
            }
        }
        return -1
    }

}

extension Int {
    var float: Float {
        return Float(self)
    }
}

extension CGPoint {
    func distanceSquaredTo(point: CGPoint) -> CGFloat {
        let dx = (point.x - self.x)
        let dy = (point.y - self.y)
        return dx*dx + dy*dy
    }
}
