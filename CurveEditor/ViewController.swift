//
//  ViewController.swift
//  CurveEditor
//
//  Created by Dishan Fernando on 1/9/18.
//  Copyright © 2018 IronOne Technologies. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var editorView: CurveEditorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        var points = [CGPoint]()
        
        let width = self.view.frame.width
        let height = self.view.frame.height
        let margin = CGFloat(40.0)
        
        let dx = (width - 2*margin) / 4
        let x0 = margin
        let x1 = margin + dx
        let x2 = margin + 2*dx
        let x3 = margin + 3*dx
        let x4 = margin + 4*dx
        
        let curveHeight = (height - 2*margin)
        let dy1 = curveHeight / 11
        let dy2 = curveHeight / 2
        let y0 = margin
        let y1 = margin + dy1
        let y2 = margin + dy2
        let y3 = margin + curveHeight - dy1
        let y4 = margin + curveHeight
        
        points.append(CGPoint(x: x2, y: y0))
        points.append(CGPoint(x: x1, y: y1))
        points.append(CGPoint(x: x0, y: y2))
        points.append(CGPoint(x: x1, y: y3))
        points.append(CGPoint(x: x2, y: y4))
        points.append(CGPoint(x: x3, y: y3))
        points.append(CGPoint(x: x4, y: y2))
        points.append(CGPoint(x: x3, y: y1))
    
        
        editorView.curvePoints = points
        editorView.showControlPoints = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

