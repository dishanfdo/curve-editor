//: Playground - noun: a place where people can play

import UIKit
//var str = "Hello, playground"

//var n = 10

private func getCoefficientMatrix(pointCount: Int) -> [[Float]]? {
    guard pointCount != 0 else { return nil }
    
    var data = [[Float]]()
    for i in 0..<pointCount {
        var row = [Float]()
        for j in 0..<pointCount {
            var val: Float = 0
            
            if (i == j) {
                val = 4
            }
                
            else if (abs(i - j) == 1 || abs(i - j) == pointCount-1) {
                val = 1
            }
            row.append(val)
        }
        data.append(row)
    }
    
    return data
}
getCoefficientMatrix(pointCount: 10)

//[10, 20]

extension CGPoint {
    static func + (left: CGPoint, right: CGPoint) -> CGPoint {
        return CGPoint(x: left.x + right.x, y: left.y + right.y)
    }
    
    static func - (left: CGPoint, right: CGPoint) -> CGPoint {
        return CGPoint(x: left.x - right.x, y: left.y - right.y)
    }
    
    static func * (left: CGPoint, right: CGFloat) -> CGPoint {
        return CGPoint(x: left.x * right, y: left.y * right)
    }
    
    static func * (left: CGPoint, right: Double) -> CGPoint {
        return CGPoint(x: left.x.double * right, y: left.y.double * right)
    }
    
    static func / (left: CGPoint, right: Double) -> CGPoint {
        return CGPoint(x: left.x.double / right, y: left.y.double / right)
    }
    
    func toArray() -> [CGFloat] {
        return [self.x, self.y]
    }
}

extension CGFloat {
    var double: Double {
        return Double(self)
    }
}

private func getResultMatrix(points: [CGPoint]) -> [[CGFloat]]? {
    let count = points.count
    guard count != 0 else { return nil }
    
    var data = [[CGFloat]]()
    for i in 0..<count {
        let j = (i + 1) % count
        
        let P1 = points[i]
        let P2 = points[j]
        
        let P = P1*4.0 + P2*2.0
        data.append(P.toArray())
    }
    
    return data
}

let points = [CGPoint(x:1, y:2), CGPoint(x:3, y:7), CGPoint(x:10, y:5), CGPoint(x:7, y:1)]
getResultMatrix(points: points)

